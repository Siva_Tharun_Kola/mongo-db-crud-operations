package com.dineshonjava.mongo.test;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.mongodb.util.JSON;

/**
 * @author Dinesh Rajput
 *
 */
public class InsertDocumentDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// connect to mongoDB, IP and port number
			Mongo mongo = new Mongo("localhost", 27017);
 
			// get database from MongoDB,
			// if database doesn't exists, mongoDB will create it automatically
			DB db = mongo.getDB("dineshonjavaDB");
			
			// get a single collection
			DBCollection collection = db.getCollection("employees");
 
			// BasicDBObject example
			System.out.println("BasicDBObject example...");
			BasicDBObject document = new BasicDBObject();
			document.put("database", "dineshonjavaDB");
			document.put("table", "employees");
 
			BasicDBObject documentDetail = new BasicDBObject();
			documentDetail.put("empId", "10001");
			documentDetail.put("empName", "Dinesh");
			documentDetail.put("salary", "70000");
			document.put("detail", documentDetail);
 
			collection.insert(document);
 
			DBCursor cursorDoc = collection.find();
			while (cursorDoc.hasNext()) {
				System.out.println(cursorDoc.next());
			}
 
			collection.remove(new BasicDBObject());
 
			// BasicDBObjectBuilder example
			System.out.println("BasicDBObjectBuilder example...");
			BasicDBObjectBuilder documentBuilder = BasicDBObjectBuilder.start()
								.add("database", "dineshonjavaDB")
                                .add("table", "employees");
 
			BasicDBObjectBuilder documentBuilderDetail = BasicDBObjectBuilder.start()
                                .add("empId", "10001")
                                .add("empName", "Dinesh")
								.add("salary", "70000");
 
			documentBuilder.add("detail", documentBuilderDetail.get());
 
			collection.insert(documentBuilder.get());
 
			DBCursor cursorDocBuilder = collection.find();
			while (cursorDocBuilder.hasNext()) {
				System.out.println(cursorDocBuilder.next());
			}
 
			collection.remove(new BasicDBObject());
 
			// Map example
			System.out.println("Map example...");
			Map<String, Object> documentMap = new HashMap<String, Object>();
			documentMap.put("database", "dineshonjavaDB");
			documentMap.put("table", "employees");
 
			Map<String, Object> documentMapDetail = new HashMap<String, Object>();
			documentMapDetail.put("empId", "10001");
			documentMapDetail.put("empName", "Dinesh");
			documentMapDetail.put("salary", "70000");
 
			documentMap.put("detail", documentMapDetail);
 
			collection.insert(new BasicDBObject(documentMap));
 
			DBCursor cursorDocMap = collection.find();
			while (cursorDocMap.hasNext()) {
				System.out.println(cursorDocMap.next());
			}
 
			collection.remove(new BasicDBObject());
 
			// JSON parse example
			System.out.println("JSON parse example...");
 
			String json = "{'database' : 'dineshonjavaDB','table' : 'employees'," +
			"'detail' : {'empId' : 10001, 'empName' : 'Dinesh', 'salary' : '70000'}}}";
 
			DBObject dbObject = (DBObject)JSON.parse(json);
 
			collection.insert(dbObject);
 
			DBCursor cursorDocJSON = collection.find();
			while (cursorDocJSON.hasNext()) {
				System.out.println(cursorDocJSON.next());
			}
 
			//collection.remove(new BasicDBObject());
 
			System.out.println("Done");
 
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}
 
	}

}
